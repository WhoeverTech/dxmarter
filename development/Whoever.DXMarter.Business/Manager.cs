﻿using System;
using System.Collections.Generic;
using System.Linq;
using Whoever.DXMarter.DataAccess;
using Whoever.DXMarter.Framework.Attributes;
using Whoever.DXMarter.Framework.Bases;
using Whoever.DXMarter.Framework.Infrastructure;
using Whoever.DXMarter.Framework.Interfaces;
using Whoever.DXMarter.Framework.Models;

namespace Whoever.DXMarter.Business
{
    public abstract class Manager<TEntity> : IDisposable, IManager<TEntity> where TEntity : BusinessEntity, new()
    {
        private readonly Repository<TEntity> _repository;
        private readonly IRelatedContext _relatedContext;

        public Repository<TEntity> Repository
        {
            get
            {
                return _repository;
            }
        }

        public bool Static { get; set; }
        public bool Related { get; set; }
        public bool InProcess { get; set; }
        public IRelatedContext RelatedContext { get { return _relatedContext; } }

        protected Manager(Repository<TEntity> repository)
        {
            _repository = repository;
            _relatedContext = DXMarterContext.RelatedContext;
        }

        public virtual TEntity ReadById(TEntity entity)
        {
            return UseContext(() => Repository.ReadById(entity));
        }

        public virtual TEntity ReadById(int id)
        {
            return UseContext(() => Repository.ReadById(id));
        }

        public virtual IEnumerable<TEntity> ReadAll()
        {
            return UseContext(() => Repository.ReadAll());
        }

        public virtual IEnumerable<TEntity> ReadAll(Pager pager)
        {
            return UseContext(() => Repository.ReadAll(pager));
        }

        public virtual IEnumerable<TEntity> ReadAllRelated()
        {
            return UseContext(() => Repository.ReadAllRelated());
        }

        public virtual IEnumerable<TEntity> ReadAllRelated(Pager pager)
        {
            return UseContext(() => Repository.ReadAllRelated(pager));
        }

        public virtual IEnumerable<TEntity> ReadFiltered(TEntity entity)
        {
            return UseContext(() => Repository.ReadFiltered(entity));
        }

        public virtual IEnumerable<TEntity> ReadFiltered(TEntity entity, Pager pager)
        {
            return UseContext(() => Repository.ReadFiltered(entity, pager));
        }

        public virtual IEnumerable<TEntity> ReadFiltered(ViewModel<TEntity> model)
        {
            return UseContext(() => Repository.ReadFiltered(model));
        }

        public virtual IEnumerable<TEntity> ReadFiltered(ViewModel<TEntity> model, Pager pager)
        {
            return UseContext(() => Repository.ReadFiltered(model, pager));
        }

        public virtual IEnumerable<TEntity> ReadFiltered(Filter filter)
        {
            return UseContext(() => Repository.ReadFiltered(filter));
        }

        public virtual IEnumerable<TEntity> ReadFiltered(Filter filter, Pager pager)
        {
            return UseContext(() => Repository.ReadFiltered(filter, pager));
        }

        public virtual IEnumerable<TEntity> ReadFiltered(Func<TEntity, bool> predicate)
        {
            return UseContext(() => Repository.ReadFiltered(predicate));
        }

        public virtual IEnumerable<TEntity> ReadFiltered(Func<TEntity, bool> predicate, Pager pager)
        {
            return UseContext(() => Repository.ReadFiltered(predicate, pager));
        }

        public void FillSelectablesRelations(ViewModel<TEntity> model)
        {
            UseContext(() =>
            {
                foreach (
                    var property in model.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(SelectableRelationshipAttribute), true).Any()))
                {
                    var setterMethod = property.GetSetMethod();
                    var attribute = property.GetCustomAttributes(typeof(SelectableRelationshipAttribute), true).First() as SelectableRelationshipAttribute;
                    var manager = Activator.CreateInstance(attribute.ManagerType);
                    var getAllMethod = manager.GetType().GetMethods().Last(m => m.Name == "GetAll");
                    setterMethod.Invoke(model, new[] { getAllMethod.Invoke(manager, new object[] { }) });
                }
            });
        }

        public virtual void Create(IEnumerable<TEntity> items)
        {
            UseContext(() => Repository.Create(items));
        }

        public virtual TEntity Create(TEntity item)
        {
            return UseContext(() => Repository.Create(item));
        }

        public virtual void Rebind(IEnumerable<TEntity> items)
        {
            UseContext(() => Repository.Rebind(items));
        }

        public virtual void Update(IEnumerable<TEntity> items)
        {
            UseContext(() => Repository.Update(items));
        }

        public virtual bool Update(TEntity item)
        {
            return UseContext(() => Repository.Update(item));
        }

        public virtual void Delete(IEnumerable<TEntity> items)
        {
            UseContext(() => Repository.Delete(items));
        }

        public virtual bool Delete(TEntity item)
        {
            return UseContext(() => Repository.Delete(item));
        }

        public void RollBack()
        {
            UseContext(() => Repository.RollBack());
        }

        public void UndoChanges(TEntity item)
        {
            UseContext(() => Repository.UndoChanges(item));
        }

        public void UndoChanges(IEnumerable<TEntity> items)
        {
            UseContext(() => Repository.UndoChanges(items));
        }

        public virtual void Dispose()
        {
            if (!Static)
                Repository.ClearContext();
        }

        public int SaveChanges()
        {
            return Repository.SaveChanges();
        }

        protected T UseContext<T>(Func<T> innerCode, bool startProcess = false)
        {
            try
            {
                Repository.Context = Repository.Context ?? Repository.ContextCreator.Create();

                ResolveInProcess(startProcess);

                var result = innerCode();

                UseContextFinalize(startProcess);

                return result;
            }
            catch
            {
                if (!Static)
                    Repository.ClearContext();
                throw;
            }
        }

        protected void UseContext(Action innerCode, bool startProcess = false)
        {
            try
            {
                Repository.Context = Repository.Context ?? Repository.ContextCreator.Create();

                ResolveInProcess(startProcess);

                innerCode();

                UseContextFinalize(startProcess);
            }
            catch
            {
                if (!Static)
                    Repository.ClearContext();
                throw;
            }
        }

        private void ResolveInProcess(bool startProcessCall)
        {
            if (InProcess)
            {
                if (startProcessCall && Repository.Context != null) RelatedContext.SetAll(Repository.Context);

                return;
            }

            InProcess = startProcessCall;
        }

        private void UseContextFinalize(bool startProcessCall)
        {
            if (Related) return;

            if (InProcess && !startProcessCall) return;

            Repository.SaveChanges();
            if (!Static)
                Repository.ClearContext();
            InProcess = false;
        }
    }
}
