﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Whoever.DXMarter.Framework.Bases;
using Whoever.DXMarter.Framework.CustomExceptions;
using Whoever.DXMarter.Framework.Infrastructure;
using Whoever.DXMarter.Framework.Interfaces;
using Whoever.DXMarter.Framework.Models;

namespace Whoever.DXMarter.DataAccess
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : BusinessEntity, new()
    {
        private readonly IDataContextCreator _contextCreator;
        private readonly QueryFactory<TEntity> _queryFactory;

        protected Repository()
        {
            _contextCreator = DXMarterContext.DataContextCreator;
            _queryFactory = new QueryFactory<TEntity>();
        }

        public DbContext Context { get; set; }
        public DbSet<TEntity> DbSet { get { return Context.Set<TEntity>(); } }

        public IDataContextCreator ContextCreator
        {
            get { return _contextCreator; }
        }

        public QueryFactory<TEntity> QueryFactory
        {
            get
            {
                _queryFactory.Context = Context;
                return _queryFactory;
            }
        }

        public virtual TEntity ReadById(TEntity entity)
        {
            return DbSet.Find(entity.Id);
        }

        public virtual TEntity ReadById(int id)
        {
            return DbSet.Find(id);
        }

        public virtual IEnumerable<TEntity> ReadAll()
        {
            return QueryFactory.ReadAll().ToList();
        }

        public virtual IEnumerable<TEntity> ReadAll(Pager pager)
        {
            return QueryFactory.ReadAll(pager).ToList();
        }

        public virtual IEnumerable<TEntity> ReadAllRelated()
        {
            return QueryFactory.ReadAllRelated().ToList();
        }

        public virtual IEnumerable<TEntity> ReadAllRelated(Pager pager)
        {
            return QueryFactory.ReadAllRelated(pager).ToList();
        }

        public virtual IEnumerable<TEntity> ReadFiltered(ViewModel<TEntity> model)
        {
            return QueryFactory.ReadFiltered(model).ToList();
        }

        public virtual IEnumerable<TEntity> ReadFiltered(ViewModel<TEntity> model, Pager pager)
        {
            return QueryFactory.ReadFiltered(model, pager).ToList();
        }

        public virtual IEnumerable<TEntity> ReadFiltered(TEntity entity)
        {
            return QueryFactory.ReadFiltered(entity).ToList();
        }

        public virtual IEnumerable<TEntity> ReadFiltered(TEntity entity, Pager pager)
        {
            return QueryFactory.ReadFiltered(entity, pager).ToList();
        }

        public virtual IEnumerable<TEntity> ReadFiltered(Filter filter)
        {
            return QueryFactory.ReadFiltered(filter).ToList();
        }

        public virtual IEnumerable<TEntity> ReadFiltered(Filter filter, Pager pager)
        {
            return QueryFactory.ReadFiltered(filter, pager).ToList();
        }

        public virtual IEnumerable<TEntity> ReadFiltered(Func<TEntity, bool> predicate)
        {
            return QueryFactory.ReadFiltered(predicate).ToList();
        }

        public virtual IEnumerable<TEntity> ReadFiltered(Func<TEntity, bool> predicate, Pager pager)
        {
            return QueryFactory.ReadFiltered(predicate, pager).ToList();
        }

        public TEntity Create(TEntity item)
        {
            return DbSet.Add(item);
        }

        public virtual void Create(IEnumerable<TEntity> items)
        {
            foreach (var item in items)
                Create(item);
        }

        public virtual void Rebind(IEnumerable<TEntity> items)
        {
            try
            {
                foreach (var contextItem in items.Select(ReadById).Where(contextItem => contextItem != null))
                {
                    Delete(contextItem);
                }

                Create(items);
            }
            catch (Exception ex)
            {
                throw new DataAccessException(ex.Message);
            }
        }

        public virtual bool Update(TEntity item)
        {
            try
            {
                if (Context.Entry(item).State == EntityState.Detached)
                {
                    var contextItem = DbSet.Find(item.Id);

                    if (contextItem == null) return false;

                    Context.Entry(contextItem).CurrentValues.SetValues(item);
                    Context.Entry(contextItem).State = EntityState.Modified;
                }
                else
                {
                    Context.Entry(item).State = EntityState.Modified;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new DataAccessException(ex.Message);
            }
        }

        public virtual void Update(IEnumerable<TEntity> items)
        {
            foreach (var item in items)
                Update(item);
        }

        public virtual bool Delete(TEntity item)
        {
            try
            {
                if (Context.Entry(item).State == EntityState.Detached)
                {
                    var contextItem = DbSet.Find(item.Id);

                    if (contextItem == null)
                        return false;

                    item = contextItem;
                }

                Context.Entry(item).State = EntityState.Deleted;
                DbSet.Remove(item);

                return true;
            }
            catch (Exception ex)
            {
                throw new DataAccessException(ex.Message);
            }
        }

        public virtual void Delete(IEnumerable<TEntity> items)
        {
            foreach (var item in items)
                Delete(item);
        }

        public virtual void RollBack()
        {
            var changedEntries =
                Context.ChangeTracker.Entries().Where(x => x.State != EntityState.Unchanged).ToList();

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Modified))
            {
                entry.CurrentValues.SetValues(entry.OriginalValues);
                entry.State = EntityState.Unchanged;
            }

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Added))
            {
                entry.State = EntityState.Detached;
            }

            foreach (var entry in changedEntries.Where(x => x.State == EntityState.Deleted))
            {
                entry.State = EntityState.Unchanged;
            }
        }

        public virtual void UndoChanges(TEntity item)
        {
            var entry = Context.Entry(item);

            entry.CurrentValues.SetValues(entry.OriginalValues);
            entry.State = EntityState.Unchanged;
        }

        public virtual void UndoChanges(IEnumerable<TEntity> items)
        {
            foreach (var item in items)
            {
                UndoChanges(item);
            }
        }

        public virtual int SaveChanges()
        {
            try
            {
                return Context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new DataAccessException(ex.Message);
            }
        }

        public virtual void ClearContext()
        {
            if (Context == null) return;

            Context.Dispose();
            Context = null;
        }
    }
}
