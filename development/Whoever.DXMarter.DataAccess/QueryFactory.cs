﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Whoever.DXMarter.Framework.Bases;
using Whoever.DXMarter.Framework.Models;

namespace Whoever.DXMarter.DataAccess
{
    public class QueryFactory<TEntity> where TEntity : BusinessEntity, new()
    {
        private DbContext _context;

        private static string EntityName
        {
            get
            {
                return typeof(TEntity).Name;
            }
        }

        private static Type EntityType
        {
            get
            {
                return typeof(TEntity);
            }
        }

        public DbContext Context
        {
            get
            {
                return _context;
            }
            set
            {
                _context = value;
            }
        }

        private object ContextBox
        {
            get
            {
                return _context;
            }
        }

        private Type ContextType
        {
            get
            {
                return ContextBox.GetType();
            }
        }

        private object DbSet
        {
            get
            {
                return _context.Set<TEntity>();
            }
        }

        private PropertyInfo DbSetProperty
        {
            get
            {
                return ContextType.GetProperty(EntityName);
            }
        }

        public virtual IQueryable<TEntity> ReadByKeys(ViewModel<TEntity> model)
        {
            var instance = Activator.CreateInstance(model.GetType());

            foreach (var property in EntityType.GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), true).Length != 0))
            {
                if (model.GetType().GetProperty(property.Name).GetValue(model, null) == null)
                    throw new ArgumentException("All the keys must be loaded");
                var setterMethod = instance.GetType().GetProperty(property.Name).GetSetMethod();
                setterMethod.Invoke(instance, new object[] { model.GetType().GetProperty(property.Name).GetValue(model, null) });
            }

            return ReadFiltered((ViewModel<TEntity>)instance);
        }

        public virtual IQueryable<TEntity> ReadByKeys(TEntity entity)
        {
            var instance = Activator.CreateInstance(EntityType);

            foreach (var property in EntityType.GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), true).Length != 0))
            {
                if (property.GetValue(entity, null) == null)
                    throw new ArgumentException("All the keys must be loaded");
                var setterMethod = instance.GetType().GetProperty(property.Name).GetSetMethod();
                setterMethod.Invoke(instance, new object[] { entity.GetType().GetProperty(property.Name).GetValue(entity, null) });
            }

            return ReadFiltered((TEntity)instance);
        }

        public virtual IQueryable<TEntity> ReadAll()
        {
            return (IQueryable<TEntity>)DbSet;
        }

        public virtual IQueryable<TEntity> ReadAll(Pager pager)
        {
            var result = ReadAll();
            return PagerFactory<TEntity>.GetPaged(result, pager);
        }

        public virtual IQueryable<TEntity> ReadAllRelated()
        {
            var virtualProperties = EntityType.GetProperties().Where(p => p.GetGetMethod().IsVirtual).ToList();
            var includeMethod = DbSetProperty.PropertyType.GetMethod("Include");
            var call = new object();

            if (virtualProperties.Any())
            {
                var first = true;
                foreach (var virtualProperty in virtualProperties)
                {
                    call = includeMethod.Invoke(first ? DbSet : call, new object[] { virtualProperty.Name.ToString() });
                    first = false;
                }
                return (IQueryable<TEntity>)call;
            }

            return (IQueryable<TEntity>)DbSet;
        }

        public virtual IQueryable<TEntity> ReadAllRelated(Pager pager)
        {
            var result = ReadAllRelated();
            return PagerFactory<TEntity>.GetPaged(result, pager);
        }

        public virtual IQueryable<TEntity> ReadFiltered(TEntity entity)
        {
            if (!EntityType.GetProperties().Any())
                throw new Exception("Entity don't have any property");

            var query = (IQueryable<TEntity>)DbSet;
            var enumerableExtensions = typeof(Enumerable);
            var whereMethod =
                enumerableExtensions.GetMethods().First(m => m.Name == "Where")
                    .MakeGenericMethod(typeof(TEntity));
            Expression whereClause = Expression.Empty();
            var expParameter = Expression.Parameter(EntityType, "e");

            var equalsMethodEmpty = Expression.Variable(typeof(bool), "Empty");
            var expLambda = Expression.Lambda<Func<TEntity, bool>>(equalsMethodEmpty, expParameter);

            var first = true;
            foreach (
                var property in
                    EntityType.GetProperties()
                        .Where(
                            p =>
                                !p.GetGetMethod().IsVirtual &&
                                !p.GetCustomAttributes(typeof(NotMappedAttribute), true).Any()))
            {
                var value = entity.GetType().GetProperty(property.Name).GetValue(entity, null);
                if (value != null && !string.IsNullOrEmpty(value.ToString()))
                {
                    Expression expProperty = Expression.Property(expParameter, property.Name);
                    Expression expTarget =
                        Expression.Constant(entity.GetType().GetProperty(property.Name).GetValue(entity, null));

                    Expression filterClause;
                    if (property.PropertyType == typeof(string))
                    {
                        expProperty = Expression.Call(expProperty, "ToLower", null);
                        expTarget = Expression.Call(expTarget, "ToLower", null);

                        var indexOfMethod = Expression.Call(expProperty, "IndexOf", null, expTarget);
                        var comparerMethod = Expression.GreaterThanOrEqual(indexOfMethod, Expression.Constant(0));

                        filterClause = comparerMethod;
                    }
                    else
                    {
                        var equalsMethod = Expression.Equal(expProperty, expTarget);

                        filterClause = equalsMethod;
                    }

                    if (first)
                    {
                        whereClause = filterClause;
                        first = false;
                    }
                    else
                    {
                        whereClause = Expression.And(whereClause, filterClause);
                    }
                }
            }

            var result = Enumerable.Empty<TEntity>();

            if (whereClause is DefaultExpression)
                result = (IEnumerable<TEntity>)DbSet;
            else
            {
                expLambda = Expression.Lambda<Func<TEntity, bool>>(whereClause, expParameter);
                result = (IEnumerable<TEntity>)whereMethod.Invoke(query, new object[] { query, expLambda.Compile() });
            }

            return result.AsQueryable<TEntity>();
        }

        public virtual IQueryable<TEntity> ReadFiltered(TEntity entity, Pager pager)
        {
            var result = ReadFiltered(entity);
            return PagerFactory<TEntity>.GetPaged(result, pager);
        }

        public virtual IQueryable<TEntity> ReadFiltered(ViewModel<TEntity> model)
        {
            if (EntityType.GetProperties().Count() == 0)
                throw new Exception("Entity don't have any property");

            var query = (IQueryable<TEntity>)DbSet;
            var enumerableExtensions = typeof(System.Linq.Enumerable);
            var whereMethod =
                enumerableExtensions.GetMethods()
                    .Where(m => m.Name == "Where")
                    .First()
                    .MakeGenericMethod(typeof(TEntity));
            Expression whereClause = Expression.Empty();
            var expParameter = Expression.Parameter(EntityType, "e");

            var expTargetEmpty = Expression.Constant(true);
            var equalsMethodEmpty = Expression.Variable(typeof(bool), "Empty");
            var expLambda = Expression.Lambda<Func<TEntity, bool>>(equalsMethodEmpty, expParameter);

            var filterProperty = model.GetType().GetProperty("Filter");
            if (filterProperty == null) throw new Exception("View model must have Filter property");
            var filterObject = model.GetType().GetProperty("Filter").GetValue(model, null);

            var first = true;
            foreach (
                var property in
                    EntityType.GetProperties()
                        .Where(
                            p =>
                                !p.GetGetMethod().IsVirtual &&
                                p.GetCustomAttributes(typeof(NotMappedAttribute), true).Count() == 0))
                if (filterObject.GetType().GetProperty(property.Name) != null)
                {
                    Expression filterClause = Expression.Empty();
                    var value = filterObject.GetType().GetProperty(property.Name).GetValue(filterObject, null);
                    if (value != null && !string.IsNullOrEmpty(value.ToString()))
                    {
                        Expression expProperty = Expression.Property(expParameter, property.Name);
                        Expression expTarget =
                            Expression.Constant(
                                filterObject.GetType().GetProperty(property.Name).GetValue(filterObject, null));

                        if (property.PropertyType == typeof(string))
                        {
                            expProperty = Expression.Call(expProperty, "ToLower", null);
                            expTarget = Expression.Call(expTarget, "ToLower", null);

                            var indexOfMethod = Expression.Call(expProperty, "IndexOf", null, expTarget);
                            var comparerMethod = Expression.GreaterThanOrEqual(indexOfMethod, Expression.Constant(0));

                            filterClause = comparerMethod;
                        }
                        else
                        {
                            var equalsMethod = Expression.Equal(expProperty, expTarget);

                            filterClause = equalsMethod;
                        }

                        if (first)
                        {
                            whereClause = filterClause;
                            first = false;
                        }
                        else
                            whereClause = Expression.And(whereClause, filterClause);
                    }
                }

            var result = Enumerable.Empty<TEntity>();

            if (whereClause is DefaultExpression)
                result = (IEnumerable<TEntity>)DbSet;
            else
            {
                expLambda = Expression.Lambda<Func<TEntity, bool>>(whereClause, expParameter);
                result = (IEnumerable<TEntity>)whereMethod.Invoke(query, new object[] { query, expLambda.Compile() });
            }

            return result.AsQueryable<TEntity>();
        }

        public virtual IQueryable<TEntity> ReadFiltered(ViewModel<TEntity> model, Pager pager)
        {
            var result = ReadFiltered(model);
            return PagerFactory<TEntity>.GetPaged(result, pager);
        }

        public virtual IQueryable<TEntity> ReadFiltered(Filter filter)
        {
            var entityProperties = EntityType.GetProperties();

            if (entityProperties.Count() == 0)
                throw new Exception("Entity don't have any property");

            var query = (IQueryable<TEntity>)DbSet;
            var enumerableExtensions = typeof(System.Linq.Enumerable);
            var whereMethod =
                enumerableExtensions.GetMethods()
                    .Where(m => m.Name == "Where")
                    .First()
                    .MakeGenericMethod(typeof(TEntity));
            Expression whereClause = Expression.Empty();
            var expParameter = Expression.Parameter(EntityType, "e");

            var equalsMethodEmpty = Expression.Variable(typeof(bool), "Empty");
            var expLambda = Expression.Lambda<Func<TEntity, bool>>(equalsMethodEmpty, expParameter);

            var filterType = filter.GetType();

            var first = true;
            foreach (
                var property in
                    entityProperties.Where(
                        p =>
                            !p.GetGetMethod().IsVirtual &&
                            p.GetCustomAttributes(typeof(NotMappedAttribute), true).Count() == 0))
                if (filterType.GetProperty(property.Name) != null)
                {
                    Expression filterClause = Expression.Empty();
                    var value = filterType.GetProperty(property.Name).GetValue(filter, null);
                    if (value != null && !string.IsNullOrEmpty(value.ToString()))
                    {
                        Expression expProperty = Expression.Property(expParameter, property.Name);
                        Expression expTarget =
                            Expression.Constant(filterType.GetProperty(property.Name).GetValue(filter, null));

                        if (property.PropertyType == typeof(string))
                        {
                            expProperty = Expression.Call(expProperty, "ToLower", null);
                            expTarget = Expression.Call(expTarget, "ToLower", null);

                            var indexOfMethod = Expression.Call(expProperty, "IndexOf", null, expTarget);
                            var comparerMethod = Expression.GreaterThanOrEqual(indexOfMethod, Expression.Constant(0));

                            filterClause = comparerMethod;
                        }
                        else
                        {
                            var equalsMethod = Expression.Equal(expProperty, expTarget);

                            filterClause = equalsMethod;
                        }

                        if (first)
                        {
                            whereClause = filterClause;
                            first = false;
                        }
                        else
                            whereClause = Expression.And(whereClause, filterClause);
                    }
                }

            var result = Enumerable.Empty<TEntity>();

            if (whereClause is DefaultExpression)
                result = (IEnumerable<TEntity>)DbSet;
            else
            {
                expLambda = Expression.Lambda<Func<TEntity, bool>>(whereClause, expParameter);
                result = (IEnumerable<TEntity>)whereMethod.Invoke(query, new object[] { query, expLambda.Compile() });
            }

            return result.AsQueryable();
        }

        public virtual IQueryable<TEntity> ReadFiltered(Filter filter, Pager pager)
        {
            var result = ReadFiltered(filter);
            return PagerFactory<TEntity>.GetPaged(result, pager);
        }

        public virtual IQueryable<TEntity> ReadFiltered(Func<TEntity, bool> predicate)
        {
            return ReadAll().Where(predicate).AsQueryable();
        }

        public virtual IQueryable<TEntity> ReadFiltered(Func<TEntity, bool> predicate, Pager pager)
        {
            var result = ReadFiltered(predicate);
            return PagerFactory<TEntity>.GetPaged(result, pager);
        }
    }
}
