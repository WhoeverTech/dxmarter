﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Whoever.DXMarter.Framework.Models;

namespace Whoever.DXMarter.DataAccess
{
    public static class PagerFactory<TEntity>
    {
        public static IQueryable<TEntity> GetPaged(IQueryable<TEntity> result, Pager pager)
        {
            if (pager.SortDir == null || pager.Page == 0 || pager.MaxResults == 0)
                throw new Exception("Pager have incorrect data");

            var defaultProperty = typeof(TEntity).GetProperties().First(p => !p.GetGetMethod().IsVirtual && !p.GetCustomAttributes(typeof(NotMappedAttribute), true).Any());

            pager.TotalResults = result.Count();
            var sortedResults = pager.SortDir == "ASC" ? result.AsQueryable().OrderBy(pager.Sort ?? defaultProperty.Name) : result.AsQueryable().OrderByDescending(pager.Sort ?? defaultProperty.Name);
            var pagedResults = sortedResults.Skip((pager.Page - 1) * pager.MaxResults).Take(pager.MaxResults);

            return pagedResults;
        }
    }
}
