﻿using Whoever.DXMarter.Aspect.Concerns;
using Whoever.DXMarter.Aspect.Interfaces;
using Whoever.DXMarter.Aspect.Models;
using Whoever.DXMarter.DataAccess;

namespace Whoever.DXMarter.Aspect.Repositories
{
    public abstract class LocalizationConcernRepository : Repository<LocalizationEntry>, ILocalizationRepository
    {
        protected LocalizationConcernRepository()
        {
            Localization.Repository = this;
        }
    }
}
