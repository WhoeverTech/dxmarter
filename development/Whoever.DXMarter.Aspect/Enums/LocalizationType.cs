﻿namespace Whoever.DXMarter.Aspect.Enums
{
    public enum LocalizationType
    {
        DisplayName,
        Action,
        Text,
        Message,
        Entity
    }
}
