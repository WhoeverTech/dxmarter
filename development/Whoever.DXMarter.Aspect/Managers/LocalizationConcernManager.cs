﻿using System.Linq;
using Whoever.DXMarter.Aspect.Concerns;
using Whoever.DXMarter.Aspect.Interfaces;
using Whoever.DXMarter.Aspect.Models;
using Whoever.DXMarter.Business;
using Whoever.DXMarter.DataAccess;

namespace Whoever.DXMarter.Aspect.Managers
{
    public abstract class LocalizationConcernManager : Manager<LocalizationEntry>, ILocalizationManager
    {
        protected LocalizationConcernManager()
            : base(Localization.Repository as Repository<LocalizationEntry>)
        {
        }

        public LocalizationEntry ReadEntry(LocalizationEntry filter)
        {
            return
                UseContext(
                    () =>
                        Repository.ReadFiltered(
                            e => e.Key == filter.Key && e.Group == filter.Group && e.Culture == filter.Culture)
                            .FirstOrDefault());
        }
    }
}
