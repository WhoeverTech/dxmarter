﻿using System;
using System.Linq.Expressions;
using Whoever.DXMarter.Aspect.Interfaces;
using Whoever.DXMarter.Aspect.Models;
using LocalizationType = Whoever.DXMarter.Aspect.Enums.LocalizationType;

namespace Whoever.DXMarter.Aspect.Concerns
{
    public static class Localization
    {
        public static ILocalizationManager Manager { get; set; }
        public static ILocalizationRepository Repository { get; set; }

        public static string GetValue(LocalizationEntry filter)
        {
            var value = string.Empty;

            if (Repository == null) return value;

            var entry = Manager.ReadEntry(filter);
            value = entry != null ? entry.Value : string.Empty;

            return value;
        }

        //public static string HtmlAttributes<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        //{
        //    var memberExpression = (MemberExpression)expression.Body;
        //    var entityName = memberExpression.Member.DeclaringType.Name;
        //    var propertyName = memberExpression.Member.Name;

        //    var group = LocalizationType.DisplayName.ToString();
        //    var key = string.Format("{0}.{1}", entityName, propertyName);

        //    return string.Format("localization-key='{0}' localization-group='{1}'", key, group);
        //}

        //public static string DisplayName<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        //{
        //    var memberExpression = (MemberExpression)expression.Body;
        //    var entityName = memberExpression.Member.DeclaringType.Name;
        //    var propertyName = memberExpression.Member.Name;

        //    var displayName = string.Empty;

        //    if (Repository != null)
        //    {
        //        var filter = new LocalizationEntry
        //        {
        //            Group = LocalizationType.DisplayName.ToString(),
        //            Key = string.Format("{0}.{1}", entityName, propertyName),
        //            Culture = CultureInfo.CurrentCulture.TwoLetterISOLanguageName
        //        };

        //        var entry = Manager.ReadEntry(filter);
        //        displayName = entry != null ? entry.Value : string.Empty;
        //    }

        //    if (!string.IsNullOrEmpty(displayName)) return displayName;

        //    var displayAttribute =
        //        (DisplayAttribute)
        //            memberExpression.Member.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault();
        //    displayName = displayAttribute != null ? displayAttribute.Name : propertyName;

        //    return displayName;
        //}

        //public static string DisplayName(string entityName, string propertyName)
        //{
        //    var displayName = string.Empty;

        //    if (Repository != null)
        //    {
        //        var filter = new LocalizationEntry
        //        {
        //            Group = LocalizationType.DisplayName.ToString(),
        //            Key = string.Format("{0}.{1}", entityName, propertyName),
        //            Culture = CultureInfo.CurrentCulture.TwoLetterISOLanguageName
        //        };

        //        var entry = Manager.ReadEntry(filter);
        //        displayName = entry != null ? entry.Value : string.Empty;
        //    }

        //    if (string.IsNullOrEmpty(displayName))
        //        displayName = propertyName;

        //    return displayName;
        //}

        //public static string Action(string actionName)
        //{
        //    if (string.IsNullOrEmpty(actionName))
        //        throw new Exception("The actionName parameter is empty");

        //    var value = Find(LocalizationType.Action.ToString(), actionName);

        //    if (string.IsNullOrEmpty(value))
        //        value = actionName;

        //    return value;
        //}

        //public static string Entity(string entityName)
        //{
        //    if (string.IsNullOrEmpty(entityName))
        //        throw new Exception("The entityName parameter is empty");

        //    var value = Find(LocalizationType.Entity.ToString(), entityName);

        //    if (string.IsNullOrEmpty(value))
        //        value = entityName;

        //    return value;
        //}

        //public static string Message(string messageName, MessageType messageType)
        //{
        //    if (string.IsNullOrEmpty(messageName))
        //        throw new Exception("The messageName parameter is empty");

        //    var key = string.Format("{0}.{1}", messageType, messageName);

        //    var value = Find(LocalizationType.Message.ToString(), key);

        //    if (string.IsNullOrEmpty(value))
        //        value = string.Format("{0}.{1}", messageType, messageName);

        //    return value;
        //}

        //public static string Text(string textName)
        //{
        //    if (string.IsNullOrEmpty(textName))
        //        throw new Exception("The textName parameter is empty");

        //    var value = Find(LocalizationType.Text.ToString(), textName);

        //    if (string.IsNullOrEmpty(value))
        //        value = textName;

        //    return value;
        //}
    }
}
