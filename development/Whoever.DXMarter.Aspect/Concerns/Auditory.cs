﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Web;
using Whoever.DXMarter.Aspect.Models;
using Whoever.DXMarter.Framework.Bases;
using Whoever.DXMarter.Framework.Enums;

namespace Whoever.DXMarter.Aspect.Concerns
{
    public class Auditory
    {


        public static AuditLog GenerateAuditLog(TypeOfCRUD typeOfCRUD, DbPropertyValues oldValues, DbPropertyValues newValues, BusinessEntity entity)
        {
            var user = HttpContext.Current.User;

            return new AuditLog
            {
                Entity = entity != null ? entity.GetType().Name : "Unknown",
                TypeOfCRUD = typeOfCRUD,
                ChangeDateTime = DateTime.Now,
                User = user != null ? user.Identity.Name : "Application",
                OldState = oldValues != null ? DescribeValues(oldValues) : string.Empty,
                NewState = newValues != null ? DescribeValues(newValues) : string.Empty
            };
        }

        private static string DescribeValues(DbPropertyValues values)
        {
            if (!values.PropertyNames.Any())
                return string.Empty;
            var sb = new StringBuilder();
            sb.Append("{");
            foreach (var prop in values.PropertyNames)
                sb.Append(string.Format("{{ {0} : '{1}' }},", prop, values[prop]));
            sb.Remove(sb.Length - 1, 1).Append("}");
            return sb.ToString();
        }
    }
}
