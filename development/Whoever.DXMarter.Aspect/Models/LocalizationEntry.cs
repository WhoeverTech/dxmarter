﻿using System.ComponentModel.DataAnnotations;
using Whoever.DXMarter.Framework.Bases;

namespace Whoever.DXMarter.Aspect.Models
{
    public class LocalizationEntry : BusinessEntity
    {
        [Required]
        public string Group { get; set; }
        [Required]
        public string Key { get; set; }
        [Required]
        public string Value { get; set; }
        [Required]
        public string Culture { get; set; }
    }
}
