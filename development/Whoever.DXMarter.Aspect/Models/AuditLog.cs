﻿using System;
using Whoever.DXMarter.Framework.Bases;
using Whoever.DXMarter.Framework.Enums;

namespace Whoever.DXMarter.Aspect.Models
{
    public class AuditLog : BusinessEntity
    {
        public string Entity { get; set; }
        public TypeOfCRUD TypeOfCRUD { get; set; }
        public DateTime ChangeDateTime { get; set; }
        public string OldState { get; set; }
        public string NewState { get; set; }
        public string User { get; set; }
    }
}
