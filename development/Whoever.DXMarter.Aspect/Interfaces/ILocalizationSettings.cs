﻿namespace Whoever.DXMarter.Aspect.Interfaces
{
    public interface ILocalizationSettings
    {
        void Initialize(ILocalizationManager manager, ILocalizationRepository repository);
    }
}
