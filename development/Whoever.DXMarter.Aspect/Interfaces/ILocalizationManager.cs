﻿using Whoever.DXMarter.Aspect.Models;

namespace Whoever.DXMarter.Aspect.Interfaces
{
    public interface ILocalizationManager : ILocalizationRepository
    {
        LocalizationEntry ReadEntry(LocalizationEntry filter);
    }
}
