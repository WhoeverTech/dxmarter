﻿using System;
using System.Runtime.Serialization;
using Whoever.DXMarter.Framework.Enums;

namespace Whoever.DXMarter.Framework.CustomExceptions
{
    [Serializable]
    public class BusinessException : Exception
    {
        public BusinessException()
            : base() { }

        public BusinessException(string message)
            : base(message) { }

        public BusinessException(string message, MessageType messageType)
            : base(message)
        {
            MessageType = messageType;
        }

        public BusinessException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public BusinessException(string message, Exception innerException)
            : base(message, innerException) { }

        public BusinessException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected BusinessException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }

        public MessageType MessageType { get; set; }
    }
}
