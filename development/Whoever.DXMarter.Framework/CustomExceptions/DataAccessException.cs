﻿using System;
using System.Runtime.Serialization;

namespace Whoever.DXMarter.Framework.CustomExceptions
{
    [Serializable]
    public class DataAccessException : Exception
    {
        public DataAccessException()
            : base() { }

        public DataAccessException(string message)
            : base(message) { }

        public DataAccessException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public DataAccessException(string message, Exception innerException)
            : base(message, innerException) { }

        public DataAccessException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected DataAccessException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
