﻿using System;
using System.Runtime.Serialization;

namespace Whoever.DXMarter.Framework.CustomExceptions
{
    [Serializable]
    public class PresentationException : Exception
    {
        public PresentationException()
            : base() { }

        public PresentationException(string message)
            : base(message) { }

        public PresentationException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public PresentationException(string message, Exception innerException)
            : base(message, innerException) { }

        public PresentationException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected PresentationException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
