﻿using System;

namespace Whoever.DXMarter.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SelectableRelationshipAttribute : Attribute
    {
        public Type EntityType { get; set; }

        public Type ManagerType { get; set; }
    }
}
