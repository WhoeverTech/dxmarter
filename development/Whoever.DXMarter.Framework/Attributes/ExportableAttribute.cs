﻿using System;

namespace Whoever.DXMarter.Framework.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public sealed class ExportableAttribute : Attribute
    {
        private readonly bool _isExportable;

        private int _order = int.MaxValue;

        private bool _isGrouped;

        private string _groupColumn = string.Empty;

        public ExportableAttribute(bool isExportable, int order)
        {
            this._isExportable = isExportable;
            this._order = order;
        }

        public ExportableAttribute(bool isExportable, int order, bool isGrouped, string groupColumn)
        {
            this._isExportable = isExportable;
            this._isGrouped = isGrouped;
            this._order = order;
            this._groupColumn = groupColumn;
        }

        public ExportableAttribute(bool isExportable)
        {
            this._isExportable = isExportable;
        }

        public ExportableAttribute()
        {
            this._isExportable = true;
        }

        public bool IsExportable
        {
            get { return _isExportable; }
        }

        public string GroupColumn
        {
            get { return _groupColumn; }
            set { this._groupColumn = value; }
        }

        public bool IsGrouped
        {
            get { return _isGrouped; }
            set { _isGrouped = value; }
        }

        public int Order
        {
            get { return _order; }
            set { this._order = value; }
        }
    }
}
