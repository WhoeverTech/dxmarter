﻿namespace System
{
    public static class TypeExtensions
    {
        public static bool IsBoolean(this Type type)
        {
            return type == typeof(bool) || type == typeof(bool?);
        }
    }
}
