﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace System
{
    public static class ObjectExtensions
    {
        public static string ToJson(this object obj)
        {
            var formatter = new IsoDateTimeConverter {DateTimeFormat = "dd-MM-yyyy"};

            var customJsonSettings = new JsonSerializerSettings()
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local
            };

            customJsonSettings.Converters.Add(formatter);

            var serialized = JsonConvert.SerializeObject(obj, customJsonSettings);

            return serialized.Replace("\\","\\\\");
        }
    }
}