﻿using System.Collections.Generic;

namespace Whoever.DXMarter.Framework.Bases
{
    public class ViewModel<TEntity>
        where TEntity : BusinessEntity, new()
    {
        public ViewModel()
        {
            List = new List<TEntity>();
            Entity = new TEntity();
        }

        public IEnumerable<TEntity> List { get; set; }
        public TEntity Entity { get; set; }
    }
}
