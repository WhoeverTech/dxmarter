﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Whoever.DXMarter.Framework.Bases
{
    public class BusinessEntity : IDisposable
    {
        [Key]
        public int Id { get; set; }

        public virtual void Dispose()
        {
        }
    }
}
