﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Whoever.DXMarter.Framework.Interfaces;

namespace Whoever.DXMarter.Framework.Bases
{
    public abstract class ApiRESTController<TEntity> : ApiController
        where TEntity : BusinessEntity, new()
    {
        private readonly IManager<TEntity> _manager;

        protected IManager<TEntity> Manager { get { return _manager; } }

        protected ApiRESTController(IManager<TEntity> manager)
        {
            _manager = manager;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Manager.ReadAllRelated();
        }

        public virtual IEnumerable<TEntity> Get()
        {
            return Manager.ReadAllRelated();
        }

        public virtual TEntity Get(int id)
        {
            var item = Manager.ReadById(id);

            if (item == null)
                throw new HttpResponseException(
                    Request.CreateResponse(HttpStatusCode.NotFound));

            return item;
        }

        public virtual HttpResponseMessage Post(TEntity entity)
        {
            if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.BadRequest, entity);

            Manager.Create(entity);
            return Request.CreateResponse(HttpStatusCode.Created, entity);
        }

        public virtual HttpResponseMessage Put(int id, TEntity entity)
        {
            if (!ModelState.IsValid) return Request.CreateResponse(HttpStatusCode.BadRequest);

            entity.Id = id;
            var result = Manager.Update(entity);

            return !result ? Request.CreateResponse(HttpStatusCode.NotFound) : Request.CreateResponse(HttpStatusCode.OK, entity);
        }

        public virtual HttpResponseMessage Delete(int id)
        {
            var entity = Manager.ReadById(id);

            if (entity == null)
                return Request.CreateResponse(HttpStatusCode.NotFound);

            try
            {
                Manager.Delete(entity);

                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}
