﻿using Whoever.DXMarter.Framework.Interfaces;

namespace Whoever.DXMarter.Framework.Infrastructure
{
    public static class DXMarterContext
    {
        public static IDataContextCreator DataContextCreator { get; set; }
        public static IRelatedContext RelatedContext { get; set; }
    }
}
