﻿namespace Whoever.DXMarter.Framework.Structs
{
    public struct KeyValueElement
    {
        public object key;
        public object value;

        public KeyValueElement(object key, object value)
        {
            this.key = key;
            this.value = value;
        }
    }
}
