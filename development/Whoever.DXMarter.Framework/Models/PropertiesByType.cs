﻿using System;

namespace Whoever.DXMarter.Framework.Models
{
    public class PropertiesByType
    {
        public Type Type { get; set; }
        public string[] Properties { get; set; }
    }
}
