﻿namespace Whoever.DXMarter.Framework.Models
{
    public class Pager
    {
        public int Page { get; set; }
        public int TotalResults { get; set; }
        public int MaxResults { get; set; }
        public string Sort { get; set; }
        public string SortDir { get; set; }

        public Pager(int maxResults)
        {
            MaxResults = maxResults;

            Page = 1;
            TotalResults = 0;
            Sort = null;
            SortDir = "ASC";
        }

        public Pager(string sort)
        {
            Sort = sort;

            Page = 1;
            TotalResults = 0;
            MaxResults = 20;
            SortDir = "ASC";
        }

        public Pager(int maxResults, string sort)
        {
            MaxResults = maxResults;
            Sort = sort;

            Page = 1;
            TotalResults = 0;
            SortDir = "ASC";
        }

        public Pager(int maxResults, string sort, string sortDir)
        {
            MaxResults = maxResults;
            Sort = sort;
            SortDir = sortDir;

            Page = 1;
            TotalResults = 0;
        }
    }
}
