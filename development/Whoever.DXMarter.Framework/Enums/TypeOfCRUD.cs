﻿namespace Whoever.DXMarter.Framework.Enums
{
    public enum TypeOfCRUD
    {
        CREATE = 0,
        READ = 1,
        UPDATE = 2,
        DELETE = 3
    }
}
