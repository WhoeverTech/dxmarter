﻿namespace Whoever.DXMarter.Framework.Enums
{
    public enum MessageType
    {
        Info = 0,
        Success = 1,
        Warning = 2,
        Error = 3
    }
}
