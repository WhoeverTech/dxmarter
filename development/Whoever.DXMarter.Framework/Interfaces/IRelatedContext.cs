﻿using System.Data.Entity;

namespace Whoever.DXMarter.Framework.Interfaces
{
    public interface IRelatedContext
    {
        void SetAll(DbContext context);
    }
}
