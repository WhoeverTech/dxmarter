﻿using System.Data.Entity;

namespace Whoever.DXMarter.Framework.Interfaces
{
    public interface IDataContextCreator
    {
        DbContext Create();
    }
}
