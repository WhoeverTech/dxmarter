﻿namespace Whoever.DXMarter.Framework.Interfaces
{
    public interface IDXMarterSettings
    {
        void Initialize(IDataContextCreator contextCreator, IRelatedContext relatedContext);
    }
}
