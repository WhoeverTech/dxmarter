﻿using System;
using System.Collections.Generic;
using Whoever.DXMarter.Framework.Bases;
using Whoever.DXMarter.Framework.Models;

namespace Whoever.DXMarter.Framework.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BusinessEntity, new()
    {
        /// <summary>
        /// Get the item by Id
        /// </summary>
        /// <returns></returns>
        TEntity ReadById(TEntity entity);

        /// <summary>
        /// Get the item by Id
        /// </summary>
        /// <returns></returns>
        TEntity ReadById(int id);

        /// <summary>
        /// Read all items from database
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> ReadAll();

        /// <summary>
        /// Read all items from database
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> ReadAll(Pager pager);

        /// <summary>
        /// Read all items and their relationships from database
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> ReadAllRelated();

        /// <summary>
        /// Read all items and their relationships from database
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> ReadAllRelated(Pager pager);

        /// <summary>
        /// Read items filtering by a loaded ViewModel corresponding to an entity of current repository
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        IEnumerable<TEntity> ReadFiltered(ViewModel<TEntity> model);

        /// <summary>
        /// Read items filtering by a loaded ViewModel corresponding to an entity of current repository
        /// </summary>
        /// <param name="model"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        IEnumerable<TEntity> ReadFiltered(ViewModel<TEntity> model, Pager pager);

        /// <summary>
        /// Read items filtering by a loaded entity corresponding to an entity of current repository (all not nullable field must be loaded)
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        IEnumerable<TEntity> ReadFiltered(TEntity entity);

        /// <summary>
        /// Read items filtering by a loaded entity corresponding to an entity of current repository (all not nullable field must be loaded)
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="pager"></param>
        /// <returns></returns>
        IEnumerable<TEntity> ReadFiltered(TEntity entity, Pager pager);

        /// <summary>
        /// Read items filtering by a loaded Filter corresponding to an entity of current repository
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> ReadFiltered(Filter filter);

        /// <summary>
        /// Read items filtering by a loaded Filter corresponding to an entity of current repository
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> ReadFiltered(Filter filter, Pager pager);

        /// <summary>
        /// Read items filtering by a predicate
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> ReadFiltered(Func<TEntity, bool> predicate);

        /// <summary>
        /// Read items filtering by a predicate
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> ReadFiltered(Func<TEntity, bool> predicate, Pager pager);

        /// <summary>
        /// Insert a new item in database
        /// </summary>
        /// <param name="item"></param>
        TEntity Create(TEntity item);

        /// <summary>
        /// Insert a list of items in databaase
        /// </summary>
        /// <param name="items"></param>
        void Create(IEnumerable<TEntity> items);

        /// <summary>
        /// Rebind list to context
        /// </summary>
        /// <param name="items"></param>
        void Rebind(IEnumerable<TEntity> items);

        /// <summary>
        /// Update a item from database
        /// </summary>
        /// <param name="item"></param>
        bool Update(TEntity item);

        /// <summary>
        /// Update a list of items from database
        /// </summary>
        /// <param name="item"></param>
        void Update(IEnumerable<TEntity> items);

        /// <summary>
        /// Delete a item from database
        /// </summary>
        /// <param name="item"></param>
        bool Delete(TEntity item);

        /// <summary>
        /// Delete a list of items from database
        /// </summary>
        /// <param name="item"></param>
        void Delete(IEnumerable<TEntity> items);

        /// <summary>
        /// Rollback all context changes
        /// </summary>
        void RollBack();

        /// <summary>
        /// Undo changes on item
        /// </summary>
        /// <param name="item"></param>
        void UndoChanges(TEntity item);

        /// <summary>
        /// Undo changes on a list of items
        /// </summary>
        /// <param name="items"></param>
        void UndoChanges(IEnumerable<TEntity> items);

        /// <summary>
        /// Save context changes
        /// </summary>
        /// <returns></returns>
        int SaveChanges();

        /// <summary>
        /// Dispose context
        /// </summary>
        void ClearContext();
    }
}
