﻿using System.Collections.Generic;
using Whoever.DXMarter.Framework.Bases;

namespace Whoever.DXMarter.Framework.Interfaces
{
    public interface IReport<in T> where T : BusinessEntity
    {
        IEnumerable<IReportModel> Execute(IEnumerable<T> list);
    }
}
