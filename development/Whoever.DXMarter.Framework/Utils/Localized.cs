﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;
using Whoever.DXMarter.Framework.Enums;

namespace Whoever.DXMarter.Framework.Utils
{
    public class Localized
    {
        private static readonly ResourceManager DisplayNameResourceManager;

        private static readonly ResourceManager ActionResourceManager;

        private static readonly ResourceManager EntityResourceManager;

        private static readonly ResourceManager TextResourceManager;

        private static readonly ResourceManager ReportingResourceManager;

        private static readonly ResourceManager MessageResourceManager;

        static Localized()
        {
            DisplayNameResourceManager = GetResourceManager("DisplayName");
            ActionResourceManager = GetResourceManager("Action");
            EntityResourceManager = GetResourceManager("Entity");
            TextResourceManager = GetResourceManager("Text");
            ReportingResourceManager = GetResourceManager("Reporting");
            MessageResourceManager = GetResourceManager("Message");
        }

        private static ResourceManager GetResourceManager(string name)
        {
            var baseName = string.Format("{0}ResourceBaseName", name);
            var assemblyName = string.Format("{0}ResourceAssembly", name);

            var resourceBaseName = ConfigurationManager.AppSettings[baseName];
            var resourceAssembly = ConfigurationManager.AppSettings[assemblyName];

            if (!string.IsNullOrEmpty(resourceBaseName) && !string.IsNullOrEmpty(resourceAssembly))
            {
                var assembly = Assembly.Load(resourceAssembly);
                var resourceManager = new ResourceManager(resourceBaseName, assembly);
                return resourceManager;
            }

            return null;
        }


        public static string DisplayName<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        {
            var displayName = string.Empty;

            var memberExpression = (MemberExpression)expression.Body;
            var entityName = memberExpression.Member.DeclaringType.Name;
            var propertyName = memberExpression.Member.Name;

            if (DisplayNameResourceManager != null)
            {
                displayName = DisplayNameResourceManager.GetString(string.Format("{0}.{1}", entityName, propertyName));
            }

            if (string.IsNullOrEmpty(displayName))
            {
                var displayAttribute = (DisplayAttribute)memberExpression.Member.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault();
                displayName = displayAttribute != null ? displayAttribute.Name : propertyName;
            }

            return displayName;
        }

        public static string DisplayName(string entityName, string propertyName)
        {
            var displayName = string.Empty;

            if (DisplayNameResourceManager != null)
            {
                displayName = DisplayNameResourceManager.GetString(string.Format("{0}.{1}", entityName, propertyName));
            }

            if (string.IsNullOrEmpty(displayName))
                displayName = propertyName;

            return displayName;
        }

        public static string Action(string actionName)
        {
            if (string.IsNullOrEmpty(actionName))
                throw new Exception("The actionName parameter is empty");

            var actionDescription = string.Empty;

            if (ActionResourceManager != null)
            {
                actionDescription = ActionResourceManager.GetString(actionName);
            }

            if (string.IsNullOrEmpty(actionDescription))
                actionDescription = actionName;

            return actionDescription;
        }

        public static string Entity(string entityName)
        {
            if (string.IsNullOrEmpty(entityName))
                throw new Exception("The entityName parameter is empty");

            var entityDescription = string.Empty;

            if (EntityResourceManager != null)
            {
                entityDescription = EntityResourceManager.GetString(entityName);
            }

            if (string.IsNullOrEmpty(entityDescription))
                entityDescription = entityName;

            return entityDescription;
        }

        public static string Message(string messageName, MessageType messageType)
        {
            if (string.IsNullOrEmpty(messageName))
                throw new Exception("The messageName parameter is empty");

            var messageDescription = string.Empty;

            if (MessageResourceManager != null)
            {
                messageDescription = MessageResourceManager.GetString(string.Format("{0}.{1}", messageType, messageName));
            }

            if (string.IsNullOrEmpty(messageDescription))
                messageDescription = string.Format("{0}.{1}", messageType, messageName);

            return messageDescription;
        }

        public static string Text(string textName)
        {
            if (string.IsNullOrEmpty(textName))
                throw new Exception("The textName parameter is empty");

            var textDescription = string.Empty;

            if (TextResourceManager != null)
            {
                textDescription = TextResourceManager.GetString(textName);
            }

            if (string.IsNullOrEmpty(textDescription))
                textDescription = textName;

            return textDescription;
        }

        public static string ReportColumn(string columnName)
        {
            if (string.IsNullOrEmpty(columnName))
                throw new Exception("The columnName parameter is empty");

            var colText = string.Empty;

            if (ReportingResourceManager != null)
            {
                colText = ReportingResourceManager.GetString(columnName);
            }

            if (string.IsNullOrEmpty(colText))
                colText = columnName;

            return colText;
        }
    }
}
