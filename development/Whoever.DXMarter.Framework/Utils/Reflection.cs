﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Whoever.DXMarter.Framework.Utils
{
    public class ReflectionHelper
    {
        public static Dictionary<PropertyInfo, object> GetAttributes(object obj, Type attributeType)
        {
            var attributes = new Dictionary<PropertyInfo, object>();

            foreach (var property in obj.GetType().GetProperties())
            {
                var attribute = property.GetCustomAttributes(attributeType, true).ToList();
                foreach (var o in attribute)
                    attributes.Add(property, o);
            }

            return attributes;
        }

        public static bool ContainAttribute(PropertyInfo property, Type attributeType)
        {
            return property.GetCustomAttributes(true).Any(attribute => attributeType.ToString() == attribute.GetType().ToString());
        }
    }
}
