﻿using System.Collections.Generic;
using Whoever.DXMarter.MVC.Results;

namespace System.Web.Mvc
{
    public static class ControllerExtensions
    {
        public static ExcelResult<T> ExcelResult<T>(this ControllerBase controller, IEnumerable<T> rows, string fileName)
        {
            return new ExcelResult<T>(rows, fileName);
        }

        public static CSVResult<T> CSVResult<T>(this ControllerBase controller, IEnumerable<T> rows, string fileName)
        {
            return new CSVResult<T>(rows, fileName);
        }
    }
}
