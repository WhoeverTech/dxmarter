﻿using System.Web.Mvc;
using Whoever.DXMarter.Framework.CustomExceptions;
using Whoever.DXMarter.Framework.Enums;
using Whoever.DXMarter.Framework.Utils;
using Whoever.DXMarter.MVC.Models;

namespace Whoever.DXMarter.MVC.Filters
{
    public class AJAXExceptionHandler : FilterAttribute, IExceptionFilter
    {
        public AJAXExceptionHandler()
        {
        }

        public AJAXExceptionHandler(AJAXResponse customResponse)
        {
            CustomResponse = customResponse;
        }

        private AJAXResponse CustomResponse { get; set; }

        public void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            string message;
            MessageType messageType;

            if (filterContext.Exception is BusinessException)
            {
                var businessException = (BusinessException)filterContext.Exception;

                message = businessException.Message;
                messageType = businessException.MessageType;
            }
            else
            {
                //message = Localized.Message("Exception", MessageType.Error);
                messageType = MessageType.Error;
            }

            filterContext.Result = new JsonResult
            {
                Data = CustomResponse ?? new AJAXResponse { Success = false, Message = message, MessageType = messageType },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}
