﻿using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Whoever.DXMarter.MVC.Utils;

namespace System.Web.Mvc
{
    public static class HtmlKOTextBoxFor
    {
        public static MvcHtmlString KOTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var propertyInfo = (PropertyInfo)memberExpression.Member;

            var binding = KO.BindingByType(propertyInfo.PropertyType, true);

            Dictionary<string, object> htmlAttributes;

            switch (binding.ToLower())
            {
                case "datepicker":
                    htmlAttributes = new Dictionary<string, object>
                    {
                        {"data-bind", string.Format("{0}: {1}", binding, propertyInfo.Name)},
                        {"class", "form-control datepicker"}
                    };
                    break;
                default:
                    htmlAttributes = new Dictionary<string, object>
                    {
                        {"data-bind", string.Format("{0}: {1}", binding, propertyInfo.Name)},
                        {"class", "form-control"}
                    };
                    break;
            }

            return htmlHelper.HTML5TextBoxFor(expression, htmlAttributes);
        }
    }
}
