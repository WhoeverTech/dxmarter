﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using Whoever.DXMarter.Framework.Bases;
using Whoever.DXMarter.Framework.Structs;

namespace System.Web.Mvc
{
    public static class HtmlKOLocalizedSelectFor
    {
        public static MvcHtmlString KOLocalizedSelectFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, IEnumerable<BusinessEntity> list, string optionsText,
            string optionsValue, string value)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var propertyName = memberExpression.Member.Name;
            var isRequired = memberExpression.Member.GetCustomAttributes(typeof(RequiredAttribute), true).Length != 0;

            var hasItem = list != null && list.Any();

            return
                new MvcHtmlString(
                    string.Format(
                        "<select data-bind=\"options: $parent.{0}, optionsText: '{1}',optionsValue: '{2}',validValue: {3}, optionsCaption: '{4}' \" {5} {6}></select>",
                        propertyName, optionsText, optionsValue, value, HtmlLocalized.Localized("SelectCaption", "Action"), hasItem ? "" : "disabled='disabled'",
                        isRequired ? "required='required'" : string.Empty));
        }

        public static MvcHtmlString KOLocalizedSelectFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, IEnumerable<KeyValueElement> list, string value)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var propertyName = memberExpression.Member.Name;
            var isRequired = memberExpression.Member.GetCustomAttributes(typeof(RequiredAttribute), true).Length != 0;

            var hasItem = list != null && list.Any();

            return
                new MvcHtmlString(
                    string.Format(
                        "<select data-bind=\"options: $parent.{0}, optionsText: '{1}',optionsValue: '{2}',validValue: {3}, optionsCaption: '{4}' \" {5} {6}></select>",
                        propertyName, "value", "key", value, HtmlLocalized.Localized("SelectCaption", "Action"),
                        hasItem ? "" : "disabled='disabled'",
                        isRequired ? "required='required'" : string.Empty));
        }
    }
}
