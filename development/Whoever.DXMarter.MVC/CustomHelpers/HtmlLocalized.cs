﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using Whoever.DXMarter.Aspect.Concerns;
using Whoever.DXMarter.Aspect.Enums;
using Whoever.DXMarter.Aspect.Models;

namespace System.Web.Mvc
{
    public static class HtmlLocalized
    {
        public static IHtmlString Localized<TModel>(this HtmlHelper<TModel> htmlHelper, string key,
            string group, string tag = null)
        {
            var culture = GetCulture();

            var filter = new LocalizationEntry
            {
                Key = key,
                Group = group,
                Culture = culture
            };

            var value = Localization.GetValue(filter);
            value = !string.IsNullOrEmpty(value) ? value : key;

            if (tag == null)
                return new MvcHtmlString(value);

            return htmlHelper.Raw(string.Format("<{0} {1}>{2}</{0}>", tag, LocalizedAttributes(key, group), value));

            return new MvcHtmlString(string.Format("<{0} {1}>{2}</{0}>", tag, LocalizedAttributes(key, group), value));
        }

        public static IHtmlString Localized<TModel>(this HtmlHelper<TModel> htmlHelper, string key,
            string group, string culture, string tag)
        {
            var cultureValue = culture ?? "en";

            var filter = new LocalizationEntry
            {
                Key = key,
                Group = group,
                Culture = cultureValue
            };

            var value = Localization.GetValue(filter);
            value = !string.IsNullOrEmpty(value) ? value : key;

            if (tag == null)
                return new MvcHtmlString(value);

            return htmlHelper.Raw(string.Format("<{0} {1}>{2}</{0}>", tag, LocalizedAttributes(key, group), value));
        }

        public static MvcHtmlString Localized(string key, string group, string tag = null)
        {
            var culture = GetCulture();

            var filter = new LocalizationEntry
            {
                Key = key,
                Group = group,
                Culture = culture
            };

            var value = Localization.GetValue(filter);
            value = !string.IsNullOrEmpty(value) ? value : key;

            if (tag == null)
                return new MvcHtmlString(value);

            return new MvcHtmlString(string.Format("<{0} {1}>{2}</{0}>", tag, LocalizedAttributes(key, group), value));
        }

        public static string DisplayName<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var entityName = memberExpression.Member.DeclaringType.Name;
            var propertyName = memberExpression.Member.Name;

            var culture = GetCulture();

            var filter = new LocalizationEntry
            {
                Group = LocalizationType.DisplayName.ToString(),
                Key = string.Format("{0}.{1}", entityName, propertyName),
                Culture = culture
            };

            var entry = Localization.GetValue(filter);
            var displayName = entry ?? string.Empty;

            if (!string.IsNullOrEmpty(displayName)) return displayName;

            var displayAttribute =
                (DisplayAttribute)
                    memberExpression.Member.GetCustomAttributes(typeof(DisplayAttribute), true).FirstOrDefault();
            displayName = displayAttribute != null ? displayAttribute.Name : propertyName;

            return displayName;
        }

        public static string DisplayName(string entityName, string propertyName)
        {
            var culture = GetCulture();

            var filter = new LocalizationEntry
            {
                Group = LocalizationType.DisplayName.ToString(),
                Key = string.Format("{0}.{1}", entityName, propertyName),
                Culture = culture
            };

            var entry = Localization.GetValue(filter);
            var displayName = entry ?? string.Empty;

            if (!string.IsNullOrEmpty(displayName)) return displayName;

            return propertyName;
        }

        public static string LocalizedAttributes<TModel, TProperty>(Expression<Func<TModel, TProperty>> expression)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var entityName = memberExpression.Member.DeclaringType.Name;
            var propertyName = memberExpression.Member.Name;

            var group = LocalizationType.DisplayName.ToString();
            var key = string.Format("{0}.{1}", entityName, propertyName);

            return LocalizedAttributes(key, group);
        }

        public static string LocalizedAttributes(string key, string group)
        {
            return string.Format("localization-key='{0}' localization-group='{1}'", key, group);
        }

        public static string GetCulture()
        {
            var cultureProvider = HttpContext.Current.Session["Culture"];
            var culture = cultureProvider != null ? cultureProvider.ToString() : string.Empty;

            return !string.IsNullOrEmpty(culture)
                ? culture
                : "en";
        }
    }
}
