﻿using System.Linq.Expressions;

namespace System.Web.Mvc
{
    public static class HtmlImage
    {
        public static MvcHtmlString Image<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, int? width = null, int? height = null, string alt = null,
            string classAttribute = null)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var propertyName = memberExpression.Member.Name;

            var dimensions = height != null && width != null
                ? string.Format("height=\"{0}\" width=\"{1}\"", height, width)
                : string.Empty;
            var altText = alt != null ? string.Format("alt=\"{0}\"", alt) : string.Empty;
            var css = classAttribute != null ? string.Format("class=\"{0}\"", classAttribute) : string.Empty;

            return
                new MvcHtmlString(string.Format("<img data-bind=\"attr:{{src:{0} }}\" {1} {2} {3} //>", propertyName,
                    dimensions, altText, css));
        }
    }
}
