﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using Whoever.DXMarter.Framework.Structs;

namespace Whoever.Patterns.MVC.CustomHelpers
{
    public static class HtmlKORadioButtonFor
    {
        public static MvcHtmlString KORadioButtonFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, IEnumerable<KeyValueElement> list, string optionsText,
            string optionsValue,
            string value, bool linear = false)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var propertyName = memberExpression.Member.Name;
            var htmlBuilder = new StringBuilder();

            htmlBuilder.AppendLine(string.Format("<div data-bind='foreach: $parent.{0}'>", propertyName));
            htmlBuilder.AppendLine(linear ? "<div class='line-option'>" : "<div>");
            htmlBuilder.AppendLine(
                string.Format("<input type='radio' data-bind='attr: {0}, radioButton : $parent.{1}' />",
                    "{value: " + optionsValue + "}", value));
            htmlBuilder.AppendLine(string.Format("<span data-bind='text: {0}'></span>", optionsText));
            htmlBuilder.AppendLine("</div>");
            htmlBuilder.AppendLine("</div>");

            return new MvcHtmlString(htmlBuilder.ToString());
        }
    }
}
