﻿using System.Linq.Expressions;

namespace System.Web.Mvc
{
    public static class HtmlLocalizedLabelFor
    {
        public static MvcHtmlString LocalizedLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var propertyName = memberExpression.Member.Name;

            return new MvcHtmlString(string.Format("<label {0} for='{1}'>{2}</label>", HtmlLocalized.LocalizedAttributes(expression), propertyName, HtmlLocalized.DisplayName(expression)));
        }
    }
}
