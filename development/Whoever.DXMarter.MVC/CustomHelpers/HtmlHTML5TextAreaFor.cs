﻿using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using Whoever.DXMarter.MVC.Utils;

namespace System.Web.Mvc
{
    public static class HtmlHTML5TextAreaFor
    {
        public static MvcHtmlString HTML5TextAreaFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, IDictionary<string, object> htmlAttributes)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var property = memberExpression.Member;

            htmlAttributes = HTML5.GetAttributesFromDataAnnotations(property, htmlAttributes);

            return htmlHelper.TextAreaFor(expression, htmlAttributes);
        }
    }
}
