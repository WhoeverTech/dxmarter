﻿using System.Collections;
using System.Globalization;
using System.Configuration;
using System.Resources;
using System.Reflection;
using System.Text;

namespace System.Web.Mvc
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString EntitiesTitle(this HtmlHelper htmlHelper)
        {
            var htmlBuilder = new StringBuilder();

            htmlBuilder.AppendLine("<script type='text/javascript'>");


            htmlBuilder.AppendLine("    var Titles = [];");

            var resourceBaseName = ConfigurationManager.AppSettings["EntityResourceBaseName"];
            var resourceAssembly = ConfigurationManager.AppSettings["EntityResourceAssembly"];

            if (!string.IsNullOrEmpty(resourceBaseName) && !string.IsNullOrEmpty(resourceAssembly))
            {
                var assembly = Assembly.Load(resourceAssembly);
                var resourceManager = new ResourceManager(resourceBaseName, assembly);

                var resourceSet = resourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
                foreach (DictionaryEntry entry in resourceSet)
                {
                    var resourceKey = entry.Key.ToString();
                    var resource = entry.Value.ToString();

                    htmlBuilder.AppendLine(string.Format("    Titles['{0}'] = '{1}';", resourceKey, resource));
                }
            }

            htmlBuilder.AppendLine("</script>");

            return new MvcHtmlString(htmlBuilder.ToString());
        }
    }
}
