﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Whoever.DXMarter.Framework.Attributes;

namespace Whoever.DXMarter.MVC.Results
{
    public class CSVResult<T> : ActionResult
    {

        private string _fileName;
        private IEnumerable<T> _rows;
        private IDictionary<String, String> _headers = null;

        public string FileName
        {
            get { return _fileName; }
        }

        public IEnumerable<T> Rows
        {
            get { return _rows; }
        }

        public CSVResult(IEnumerable<T> rows, string fileName)
        {
            if (rows == null)
            {
                throw new ArgumentNullException("rows");
            }

            //if (rows.Count == 0)
            //{
            //    throw new ArgumentException("Collection is empty", "rows");
            //}

            _rows = rows;
            _fileName = fileName.ToUpperInvariant().EndsWith("CSV") ? fileName : string.Format("{0}.csv", fileName);
        }

        public override void ExecuteResult(ControllerContext context)
        {
            // Create HtmlTextWriter
            StringWriter sw = new StringWriter();

            // Build HTML Table from Items

            // Generate headers from table

            _headers = GetColumnHeaders();

            // Create Header Row
            var headerBuilder = new StringBuilder();

            foreach (String header in _headers.Values)
            {
                headerBuilder.Append(string.Format("{0};", header));
            }
            var asd = headerBuilder.ToString().Remove(headerBuilder.ToString().Length - 1, 1);

            sw.WriteLine(headerBuilder.ToString().Remove(headerBuilder.ToString().Length - 1, 1));

            // Create Data Rows
            foreach (Object row in _rows)
            {
                var rowBuilder = new StringBuilder();

                foreach (string propName in _headers.Keys)
                {
                    Type type = row.GetType();
                    PropertyInfo property = type.GetProperty(propName);
                    IConvertible rawValue = (IConvertible)property.GetValue(row, null);
                    string strValue = null;
                    if (rawValue != null)
                    {
                        switch (rawValue.GetTypeCode())
                        {
                            case TypeCode.Boolean:
                                if ((bool)rawValue)
                                {
                                    strValue = "Sí";
                                }
                                else
                                {
                                    strValue = "No";
                                }
                                break;
                            case TypeCode.DateTime:
                                strValue = ((DateTime)rawValue).ToString("dd/MM/yyyy");
                                break;
                            case TypeCode.Decimal:
                                strValue = ((Decimal)rawValue).ToString("c");
                                break;
                            case TypeCode.Double:
                                DataTypeAttribute[] att = (DataTypeAttribute[])property.GetCustomAttributes(typeof(DataTypeAttribute), false);
                                if (att.Length == 1)
                                {
                                    switch (att[0].DataType)
                                    {
                                        case DataType.Currency:
                                            strValue = ((Decimal)rawValue).ToString("c");
                                            break;
                                        default:
                                            strValue = rawValue.ToString();
                                            break;
                                    }
                                }
                                break;
                            default:
                                strValue = rawValue.ToString();
                                break;
                        }
                    }
                    else
                    {
                        strValue = String.Empty;
                    }
                    rowBuilder.Append(string.Format("{0};", strValue));
                }
                sw.WriteLine(rowBuilder.ToString().Remove(rowBuilder.ToString().Length - 1, 1));
            }

            WriteFile(_fileName, "csv file", sw.ToString());
        }

        private IDictionary<String, String> GetColumnHeaders()
        {
            Type objectType = typeof(T);
            PropertyInfo[] properties = objectType.GetProperties();
            IDictionary<String, String> result = new Dictionary<String, String>(properties.Length);

            foreach (var prop in properties)
            {
                var exportableAttribs = (ExportableAttribute[])prop.GetCustomAttributes(typeof(ExportableAttribute), false);
                if (exportableAttribs.Length > 0 && exportableAttribs[0].IsExportable)
                {
                    DisplayAttribute[] displayAttributes = (DisplayAttribute[])prop.GetCustomAttributes(typeof(DisplayAttribute), false);
                    if (displayAttributes.Length == 1)
                    {
                        result.Add(prop.Name, displayAttributes[0].GetName());
                        continue;
                    }

                    DisplayNameAttribute[] displayNameAttributes = (DisplayNameAttribute[])prop.GetCustomAttributes(typeof(DisplayNameAttribute), false);
                    if (displayNameAttributes.Length == 1)
                    {
                        result.Add(prop.Name, displayNameAttributes[0].DisplayName);
                        continue;
                    }

                    result.Add(prop.Name, prop.Name);
                }
            }

            return result;
        }

        private static string ReplaceSpecialCharacters(string value)
        {
            value = value.Replace("’", "'");
            value = value.Replace("“", "\"");
            value = value.Replace("”", "\"");
            value = value.Replace("–", "-");
            value = value.Replace("…", "...");
            return value;
        }

        private static void WriteFile(string fileName, string contentType, string content)
        {
            var context = HttpContext.Current;
            context.Response.Clear();
            context.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.ContentType = contentType;
            context.Response.Write(content);
            context.Response.End();
        }
    }
}
