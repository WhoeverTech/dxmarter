﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Whoever.DXMarter.Framework.Attributes;
using Whoever.DXMarter.Framework.Utils;
using Whoever.DXMarter.MVC.Models;

namespace Whoever.DXMarter.MVC.Results
{
    public class ExcelResult<T> : ActionResult, IDisposable
    {
        private string _fileName;
        private IEnumerable<T> _rows;
        private IDictionary<String, String> _headers = null;

        private List<ExcelGroupColumn> _headersGroupColumns = null;

        private TableStyle _tableStyle;
        private TableItemStyle _headerStyle;
        private TableItemStyle _itemStyle;

        public string FileName
        {
            get { return _fileName; }
        }

        public IEnumerable<T> Rows
        {
            get { return _rows; }
        }

        public ExcelResult(IEnumerable<T> rows, string fileName)
        {
            if (rows == null)
            {
                throw new ArgumentNullException("rows");
            }

            _rows = rows;
            _fileName = fileName.ToUpperInvariant().EndsWith("XLS") ? fileName : fileName + ".xls";
            _tableStyle = new TableStyle();
            _headerStyle = new TableItemStyle();
            _itemStyle = new TableItemStyle();
            _itemStyle.BorderColor = Color.Black;
            _itemStyle.BorderWidth = Unit.Pixel(1);

            _tableStyle = new TableStyle();
            _tableStyle.BorderStyle = BorderStyle.Solid;
            _tableStyle.BorderColor = Color.Black;
            _tableStyle.BorderWidth = Unit.Pixel(1);

            _headerStyle = new TableItemStyle();
            _headerStyle.BackColor = Color.LightGray;
            _headerStyle.BorderStyle = BorderStyle.Solid;
            _headerStyle.BorderColor = Color.Black;
            _headerStyle.BorderWidth = Unit.Pixel(1);
        }

        public override void ExecuteResult(ControllerContext context)
        {
            // Create HtmlTextWriter
            StringWriter sw = new StringWriter();
            HtmlTextWriter tw = new HtmlTextWriter(sw);

            // Build HTML Table from Items
            if (_tableStyle != null)
            {
                _tableStyle.AddAttributesToRender(tw);
            }
            tw.RenderBeginTag(HtmlTextWriterTag.Table);

            //Generate header group columns
            _headersGroupColumns = GetHeaderGroupColumns();

            // Generate headers from table
            _headers = GetColumnHeaders();

            tw.RenderBeginTag(HtmlTextWriterTag.Thead);

            // Create Group Columns
            if (_headersGroupColumns.Count > 0)
            {
                tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                foreach (var header in _headersGroupColumns)
                {
                    if (_headerStyle != null)
                    {
                        _headerStyle.AddAttributesToRender(tw);
                    }
                    tw.AddAttribute("colspan", header.Colspan.ToString());
                    tw.RenderBeginTag(HtmlTextWriterTag.Th);
                    tw.Write(HttpUtility.HtmlEncode(HtmlLocalized.Localized(header.Name, "ReportColumn")));
                    tw.RenderEndTag();
                    tw.Flush();
                }
                tw.RenderEndTag();
            }

            // Create Header Row
            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
            foreach (String header in _headers.Values)
            {
                if (_headerStyle != null)
                {
                    _headerStyle.AddAttributesToRender(tw);
                }
                tw.RenderBeginTag(HtmlTextWriterTag.Th);
                tw.Write(HttpUtility.HtmlEncode(HtmlLocalized.Localized(header, "ReportColumn")));
                tw.RenderEndTag();
            }
            tw.RenderEndTag();

            tw.RenderEndTag();

            // Create Data Rows
            tw.RenderBeginTag(HtmlTextWriterTag.Tbody);
            foreach (Object row in _rows)
            {
                tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                foreach (string propName in _headers.Keys)
                {
                    Type type = row.GetType();
                    PropertyInfo property = type.GetProperty(propName);
                    IConvertible rawValue = (IConvertible)property.GetValue(row, null);
                    string strValue = null;
                    if (rawValue != null)
                    {
                        switch (rawValue.GetTypeCode())
                        {
                            case TypeCode.Boolean:
                                if ((bool)rawValue)
                                {
                                    strValue = "Sí";
                                }
                                else
                                {
                                    strValue = "No";
                                }
                                break;
                            case TypeCode.DateTime:
                                strValue = ((DateTime)rawValue).ToString("dd/MM/yyyy");
                                break;
                            case TypeCode.Decimal:
                                strValue = ((Decimal)rawValue).ToString("c");
                                break;
                            case TypeCode.Double:
                                DataTypeAttribute[] att = (DataTypeAttribute[])property.GetCustomAttributes(typeof(DataTypeAttribute), false);
                                if (att.Length == 1)
                                {
                                    switch (att[0].DataType)
                                    {
                                        case DataType.Currency:
                                            strValue = ((Decimal)rawValue).ToString("c");
                                            break;
                                        default:
                                            strValue = rawValue.ToString();
                                            break;
                                    }
                                }
                                break;
                            default:
                                strValue = rawValue.ToString();
                                break;
                        }
                    }
                    else
                    {
                        strValue = String.Empty;
                    }
                    if (_itemStyle != null)
                    {
                        _itemStyle.AddAttributesToRender(tw);
                    }
                    tw.RenderBeginTag(HtmlTextWriterTag.Td);
                    tw.Write(HttpUtility.HtmlEncode(strValue));
                    tw.RenderEndTag();
                }
                tw.RenderEndTag();
            }
            tw.RenderEndTag(); // tbody

            tw.RenderEndTag(); // table
            WriteFile(_fileName, "application/ms-excel", sw.ToString());
        }

        private List<ExcelGroupColumn> GetHeaderGroupColumns()
        {
            Type objectType = typeof(T);
            PropertyInfo[] properties = objectType.GetProperties();

            var list = new List<ExcelGroupColumn>();
            var result = new List<ExcelGroupColumn>();

            foreach (var prop in properties)
            {
                var exportableAttribs = (ExportableAttribute[])prop.GetCustomAttributes(typeof(ExportableAttribute), false);

                if (exportableAttribs.Length > 0 && exportableAttribs[0].IsExportable && exportableAttribs[0].IsGrouped)
                {
                    list.Add(new ExcelGroupColumn() { Name = exportableAttribs[0].GroupColumn, Order = exportableAttribs[0].Order });
                }
            }

            list = list.OrderBy(h => h.Order).ToList();

            var columnNames = list.Select(x => x.Name).Distinct().ToList();

            foreach (var col in columnNames)
            {
                var colspan = list.Count(x => x.Name == col);
                var column = list.FirstOrDefault(x => x.Name == col);

                if (column != null)
                {
                    column.Colspan = colspan;
                    result.Add(column);
                }
            }

            return result;
        }

        private IDictionary<String, String> GetColumnHeaders()
        {
            Type objectType = typeof(T);
            PropertyInfo[] properties = objectType.GetProperties();
            IDictionary<String, Tuple<String, int>> result = new Dictionary<String, Tuple<String, int>>(properties.Length);

            foreach (var prop in properties)
            {
                var exportableAttribs = (ExportableAttribute[])prop.GetCustomAttributes(typeof(ExportableAttribute), false);
                if (exportableAttribs.Length > 0 && exportableAttribs[0].IsExportable)
                {
                    DisplayAttribute[] displayAttributes = (DisplayAttribute[])prop.GetCustomAttributes(typeof(DisplayAttribute), false);
                    if (displayAttributes.Length == 1)
                    {
                        result.Add(prop.Name, Tuple.Create(displayAttributes[0].GetName(), exportableAttribs[0].Order));
                        continue;
                    }

                    DisplayNameAttribute[] displayNameAttributes = (DisplayNameAttribute[])prop.GetCustomAttributes(typeof(DisplayNameAttribute), false);
                    if (displayNameAttributes.Length == 1)
                    {
                        result.Add(prop.Name, Tuple.Create(displayNameAttributes[0].DisplayName, exportableAttribs[0].Order));
                        continue;
                    }

                    result.Add(prop.Name, Tuple.Create(prop.Name, exportableAttribs[0].Order));
                }
            }

            return result.OrderBy(h => h.Value.Item2).ToDictionary(h => h.Key, h => h.Value.Item1);
        }

        private static void ReplaceSpecialCharacters(ref string value)
        {
            value = value.Replace("’", "'");
            value = value.Replace("“", "\"");
            value = value.Replace("”", "\"");
            value = value.Replace("–", "-");
            value = value.Replace("…", "...");
        }

        private static void WriteFile(string fileName, string contentType, string content)
        {
            var context = HttpContext.Current;
            context.Response.Clear();
            context.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.ContentType = contentType;
            context.Response.Write(content);
            context.Response.End();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
