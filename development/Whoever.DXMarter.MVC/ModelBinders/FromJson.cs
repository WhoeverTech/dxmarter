﻿using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Whoever.DXMarter.MVC.ModelBinders
{
    public class FromJson : CustomModelBinderAttribute
    {
        private readonly static JavaScriptSerializer serializer = new JavaScriptSerializer();

        public override IModelBinder GetBinder()
        {
            return new JsonModelBinder();
        }

        private class JsonModelBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                var stringified = controllerContext.HttpContext.Request[bindingContext.ModelName];
                return string.IsNullOrEmpty(stringified) ? null : serializer.Deserialize(stringified, bindingContext.ModelType);
            }
        }
    }
}
