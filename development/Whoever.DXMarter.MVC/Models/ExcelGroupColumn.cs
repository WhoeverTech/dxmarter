﻿namespace Whoever.DXMarter.MVC.Models
{
    public class ExcelGroupColumn
    {
        public string Name { get; set; }

        public int Order { get; set; }

        public int Colspan { get; set; }
    }
}
