﻿using Whoever.DXMarter.Framework.Enums;

namespace Whoever.DXMarter.MVC.Models
{
    public class AJAXResponse
    {
        public object Data { get; set; }
        public bool Success { get; set; }
        public MessageType MessageType { get; set; }
        public string Message { get; set; }
        public bool MustRedirect { get; set; }
        public string RedirectURL { get; set; }
        public object ExtraData { get; set; }
    }
}
