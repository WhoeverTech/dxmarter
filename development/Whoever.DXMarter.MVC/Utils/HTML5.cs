﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Whoever.DXMarter.MVC.Utils
{
    public class HTML5
    {
        public static IDictionary<string, object> GetAttributesFromDataAnnotations(MemberInfo property,
            IDictionary<string, object> attributes)
        {
            if (property == null)
                throw new Exception("property must be sended");

            var prop = (PropertyInfo)property;

            var type = prop.PropertyType;

            var typeCode = Type.GetTypeCode(type);

            var isBoolean = prop.PropertyType.IsBoolean();

            var isNumeric = prop.PropertyType.IsValueType;

            var isDateTime = typeCode == TypeCode.DateTime;

            var isRequired =
                property.GetCustomAttributes(typeof(RequiredAttribute), true).Length != 0;

            var hasMaxLengh = property.GetCustomAttributes(typeof(MaxLengthAttribute), true).Length != 0;

            var hasDataType = property.GetCustomAttributes(typeof(DataTypeAttribute), true).Length != 0 || isBoolean || isNumeric;

            var hasPattern = property.GetCustomAttributes(typeof(RegularExpressionAttribute), true).Length != 0;

            var hasRange = property.GetCustomAttributes(typeof(RangeAttribute), true).Length != 0;

            if (isRequired)
                attributes.Add("required", "required");

            if (hasMaxLengh)
            {
                var attribute = property.GetCustomAttributes(typeof(MaxLengthAttribute), true).FirstOrDefault() as MaxLengthAttribute;
                attributes.Add("maxlength", attribute.Length);
            }

            if (hasDataType)
            {
                var attribute = property.GetCustomAttributes(typeof(DataTypeAttribute), true).FirstOrDefault() as DataTypeAttribute;

                var dataType = string.Empty;

                if (attribute != null)
                {
                    dataType = attribute.GetDataTypeName().ToLower();

                    dataType = attribute.DataType == DataType.EmailAddress ? "email" : dataType;
                    dataType = attribute.DataType == DataType.Date || attribute.DataType == DataType.DateTime
                        ? string.Empty
                        : dataType;
                }

                dataType = isBoolean ? "checkbox" : dataType;

                if (isNumeric && !hasRange && !isDateTime && !isBoolean)
                {
                    dataType = "numeric";

                    switch (typeCode)
                    {
                        case TypeCode.Decimal:
                            attributes.Add("max", decimal.MaxValue.ToString());
                            attributes.Add("min", decimal.MinValue.ToString());
                            attributes.Add("pattern", "[-+]?[0-9]*[.,]?[0-9]+");
                            attributes.Add("maxlength", decimal.MinValue.ToString().Length);
                            break;
                        case TypeCode.Double:
                            attributes.Add("max", double.MaxValue.ToString());
                            attributes.Add("min", double.MinValue.ToString());
                            attributes.Add("pattern", "^[0-9]*$");
                            attributes.Add("maxlength", double.MinValue.ToString().Length);
                            break;
                        case TypeCode.Int32:
                            attributes.Add("max", int.MaxValue.ToString());
                            attributes.Add("min", int.MinValue.ToString());
                            attributes.Add("pattern", "^[0-9]*$");
                            attributes.Add("maxlength", int.MinValue.ToString().Length);
                            break;
                        case TypeCode.Int16:
                            attributes.Add("max", short.MaxValue.ToString());
                            attributes.Add("min", short.MinValue.ToString());
                            attributes.Add("pattern", "^[0-9]*$");
                            attributes.Add("maxlength", short.MinValue.ToString().Length);
                            break;
                    }
                }

                attributes.Add("type", dataType);
            }

            if (hasPattern)
            {
                var attribute = property.GetCustomAttributes(typeof(RegularExpressionAttribute), true).FirstOrDefault() as RegularExpressionAttribute;

                attributes.Add("pattern", attribute.Pattern);
            }

            if (hasRange)
            {
                var attribute = property.GetCustomAttributes(typeof(RangeAttribute), true).FirstOrDefault() as RangeAttribute;

                int min;
                int max;

                if (int.TryParse(attribute.Minimum.ToString(), out min) &&
                    int.TryParse(attribute.Maximum.ToString(), out max))
                {
                    //'numeric' input type to prevent browser issues
                    if ((min == 0 || min == int.MinValue) && max == int.MaxValue)
                        attributes.Add("type", "numeric");
                    else
                    {
                        attributes.Add("type", "range");
                        attributes.Add("min", min);
                        attributes.Add("max", max);
                    }
                }
            }

            return attributes;
        }
    }
}
