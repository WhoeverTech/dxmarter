﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Whoever.DXMarter.Framework.Structs;

namespace Whoever.DXMarter.MVC.Utils
{
    public class Enumerator
    {
        public static IEnumerable<KeyValueElement> ToList<T>()
        {
            return from object element in Enum.GetValues(typeof(T)) select new KeyValueElement(element, HtmlLocalized.DisplayName(typeof(T).Name, element.ToString()));
        }
    }
}
