﻿using System;

namespace Whoever.DXMarter.MVC.Utils
{
    public static class KO
    {
        public static string BindingByType(Type type, bool mustValidation)
        {
            var result = mustValidation ? "validValue" : "value";

            result = typeof(DateTime).IsAssignableFrom(type) || typeof(DateTime?).IsAssignableFrom(type) ? "datepicker" : result;
            result = typeof(bool).IsAssignableFrom(type) || typeof(bool?).IsAssignableFrom(type) ? "validChecked" : result;

            return result;
        }
    }
}
